/*
 *    micro-dcl
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <gtkmm.h>

#include <iostream>
#include <algorithm>
#include <filesystem>

#include <far/Log.hpp>
#include <far/File.hpp>

Gtk::Window* win = nullptr;
Gtk::Box* BOX_ITEMS = nullptr;
Gtk::Box* BOX_NAMES = nullptr;
Gtk::Entry* ENTRY_ITEM_NAME = nullptr;
Gtk::Dialog* current_dialog = nullptr;
Glib::RefPtr<Gtk::Application> app;

struct list_item {
    Gtk::Box   content;
    Gtk::Label name;
    Gtk::Box   checks;
    Gtk::Button btn_check;
    Gtk::Button btn_uncheck;

    sigc::connection check_connection,
                     uncheck_connection;

    std::vector<Gtk::Label> check_widgets;

    uint32_t type;
};

std::vector<std::shared_ptr<list_item>> items;

void save();
void load();

void remove_check(std::shared_ptr<list_item> al_i) {
    if(!al_i) {
        return; }

    if(al_i->check_widgets.size() > 0) {
        al_i->check_widgets.pop_back();
    }
}

void add_check(std::shared_ptr<list_item> al_i) {
    if(!al_i) {
        return; }
    
    al_i->check_widgets.push_back(Gtk::Label("●"));
    al_i->checks.pack_start(al_i->check_widgets[al_i->check_widgets.size() - 1], Gtk::PACK_SHRINK);
    al_i->checks.show_all();
}

bool find_item(const std::string& as_name) {
    auto vi_iter = std::find_if(items.begin(), items.end(), [=](auto& a_item) {
        return a_item && (a_item->name.get_text() == as_name);
    });

    return vi_iter != items.end();
}

void remove_item(const std::string& as_name) {
    auto vi_iter = std::find_if(items.begin(), items.end(), [=](auto& a_item) {
        return a_item && (a_item->name.get_text() == as_name);
    });

    if(vi_iter != items.end()) {
        auto item = *vi_iter;
        item->check_connection.disconnect();
        item->uncheck_connection.disconnect();
        items.erase(vi_iter);
    }

    save();
}

void add_new_item(const std::string& as_data, unsigned long long al_checks = 0, uint16_t al_type = 0) {
    if(find_item(as_data)) {
        return;
    }

    std::shared_ptr<list_item> l(new list_item);
    //l->content.pack_start(l->name, Gtk::PACK_SHRINK, 20);
    l->type = al_type;
    l->name.set_text(as_data);

    switch(al_type) {
    default:
    case 0:
        l->btn_check.set_label("  +");
        l->btn_check.get_style_context()->add_class("BUTTON_PLUS");
        l->btn_check.set_valign(Gtk::ALIGN_CENTER);
        l->check_connection = l->btn_check.signal_clicked().connect([=]() {
            add_check(l);
            save(); });
        l->btn_uncheck.set_label("⎯  ");
        l->btn_uncheck.set_valign(Gtk::ALIGN_CENTER);
        l->btn_uncheck.get_style_context()->add_class("BUTTON_MINUS");
        l->uncheck_connection = l->btn_uncheck.signal_clicked().connect([=]() {
            remove_check(l);
            save(); });

        l->content.pack_start(l->btn_check, false, false);
        l->content.pack_start(l->btn_uncheck, false, false);

        l->btn_check.set_size_request(30, 25);
        l->btn_uncheck.set_size_request(30, 25);

        l->content.pack_start(l->checks, Gtk::PACK_SHRINK, 20);

        for(unsigned long long i = 0; i < al_checks; ++i) {
            add_check(l);
        }

        l->name.set_size_request(-1, 30);
        l->content.set_size_request(-1, 30);
        break;
    case 1:
        l->name.get_style_context()->add_class("ITEM_HEADER");
        l->content.get_style_context()->add_class("ITEM_HEADER_EMPTY");

        l->name.set_size_request(-1, 25);
        l->content.set_size_request(-1, 25);
        break;
    }

    BOX_ITEMS->pack_start(l->content);
    BOX_ITEMS->show_all();
    BOX_NAMES->pack_start(l->name);
    BOX_NAMES->show_all();

    items.push_back(l);
}

void open_error_dialog(const std::string& a_message) {
    Log(a_message);

    if(current_dialog) {
        current_dialog->hide();
    }

    Gtk::MessageDialog* d = new Gtk::MessageDialog(*win, a_message);
    d->signal_hide().connect([](){
        delete (Gtk::MessageDialog*)current_dialog;
        current_dialog = nullptr;
    });
    d->show_all();
    
    current_dialog = d;
}

std::string home_dir = "";

void create_directories() {
    DLog("Creating Directories");
    try {
        home_dir = getenv("HOME");
        home_dir += "/";
        std::filesystem::create_directories(home_dir + ".local/share/udcl");
        DLog("Created new directories");
    } catch(std::exception& e) {
        Log("Could not create new directories");
        //open_error_dialog(std::string("Failed to create essential directory '~/.local/share/udcl'! ") + e.what() + " List will not save!");
    }
}

void save() {
    File f(home_dir + ".local/share/udcl/data.dcl", std::ios::out);
    
    if(!f.is_open()) {
        Log("Failed to open file for writing! 'data.dcl'", LOG_ERROR);
        open_error_dialog("Failed to open UDCL file for writing!");
        return;
    }

    for(auto& i : items) {
        if(i) {
            std::string name = i->name.get_text();
            f.write(name.c_str(), name.size() + 1);
            uint32_t n_checks = i->check_widgets.size();
            f.write((char*)&n_checks, sizeof(uint32_t));
            uint32_t is_header = i->type;
            f.write((char*)&is_header, sizeof(uint32_t));
        }
    }

    f.close();
}

void load() {
    File f(home_dir + ".local/share/udcl/data.dcl", std::ios::in);

    if(f.is_open()) {
        char c;
        f.get(c);

        std::string strbuf = "";
        bool read = true;
        while(read) {
            strbuf += c;
            f.get(c);

            if(c == '\0') {
                uint32_t count = 0;
                f.read((char*)&count, sizeof(uint32_t));
                uint32_t is_header = 0;
                f.read((char*)&is_header, sizeof(uint32_t));
                add_new_item(strbuf, count, is_header);

                strbuf = "";

                f.get(c);
                if(c == '\0' || f.eof()) {
                    read = false;
                }
            }
        }
    }
}

void on_button_clicked() {
  Log("Window Button Pressed");
  if (win)
    win->hide();
}

void on_app_activate() {
  auto refBuilder = Gtk::Builder::create();
  try {
    refBuilder->add_from_file("main.glade");
  }
  catch(const Glib::FileError& ex) {
    std::cerr << "FileError: " << ex.what() << std::endl;
    return;
  }
  catch(const Glib::MarkupError& ex) {
    std::cerr << "MarkupError: " << ex.what() << std::endl;
    return;
  }
  catch(const Gtk::BuilderError& ex) {
    std::cerr << "BuilderError: " << ex.what() << std::endl;
    return;
  }

  refBuilder->get_widget<Gtk::Window>("MAIN_WINDOW", win);
  if (!win) {
    std::cerr << "Could not get the main window" << std::endl;
    return;
  }

  auto css = Gtk::CssProvider::create();
    try {
    if(!css->load_from_path("main.css")) {
        std::cerr << "Heck looks like the CSS didn't load!\n";
    } else {
        auto screen = Gdk::Screen::get_default();
        auto ctx = win->get_style_context();
        ctx->add_provider_for_screen(screen, css, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }
  } catch(Glib::Error& e) {
      std::cerr << e.what() <<"\n";
  }

  refBuilder->get_widget<Gtk::Entry>("ENTRY_ITEM_NAME", ENTRY_ITEM_NAME);
  refBuilder->get_widget<Gtk::Box>("BOX_ITEMS", BOX_ITEMS);
  refBuilder->get_widget<Gtk::Box>("BOX_NAMES", BOX_NAMES);
  if(!BOX_ITEMS || !BOX_NAMES) {
      open_error_dialog("A Gtk::Box widget with a name of 'BOX_ITEMS' and 'BOX_NAMES' is required.");
  } else {
    Gtk::Button* pButton = nullptr;
    refBuilder->get_widget<Gtk::Button>("BUTTON_ADD", pButton);
    if (pButton)
        pButton->signal_clicked().connect([] () {
            if(ENTRY_ITEM_NAME && !ENTRY_ITEM_NAME->get_buffer()->get_text().empty()) {
                add_new_item(ENTRY_ITEM_NAME->get_buffer()->get_text());
            } else {
                add_new_item("New Item");
            }
            save();
        });
    
    refBuilder->get_widget<Gtk::Button>("BUTTON_ADD_HEADER", pButton);
    if (pButton)
        pButton->signal_clicked().connect([] () {
            if(ENTRY_ITEM_NAME && !ENTRY_ITEM_NAME->get_buffer()->get_text().empty()) {
                add_new_item(ENTRY_ITEM_NAME->get_buffer()->get_text(), 0, 1);
            } else {
                add_new_item("New Header", 0, 1);
            }
            save();
        });

    refBuilder->get_widget<Gtk::Button>("BUTTON_REMOVE", pButton);
    if (pButton)
        pButton->signal_clicked().connect([] () { remove_item(ENTRY_ITEM_NAME->get_buffer()->get_text()); });
  }

  win->signal_hide().connect([] () { delete win; });

  app->add_window(*win);
  win->show();

  load();
}

int main(int argc, char** argv) {
  create_directories();
;
  app = Gtk::Application::create("youka.micro.dcl");
  app->signal_activate().connect([] () { on_app_activate(); });

  return app->run(argc, argv);
}
